<?php
/** @var $module \Dudley\Patterns\Pattern\SocialMediaAccounts\SocialMediaAccounts */
/** @var $item \Dudley\Patterns\Pattern\SocialMediaAccounts\SocialMediaAccountsItem */
?>

<section class="social-media-accounts">
	<ul class="social-media-accounts__list">
		<?php foreach ( $module->get_items() as $item ) : ?>
			<li class="social-media-accounts__item">
				<a class="social-media-accounts__link social-media-accounts__link--<?php $item->slug(); ?>"
				   href="<?php $item->url(); ?>"
				   target="_blank">
					<span class="social-media-accounts__link-text"><?php $item->name(); ?></span>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</section><!-- .social-media-accounts -->
