<?php

namespace Dudley\Patterns\Pattern\SocialMediaAccounts;

use Dudley\Patterns\Abstracts\AbstractRepeater;

/**
 * Class SocialMediaAccounts
 * @package Dudley\Patterns\Pattern\SocialMediaAccounts
 */
class SocialMediaAccounts extends AbstractRepeater
{
	/**
	 * @var string
	 */
	public static $action_name = 'social_media_accounts';

	/**
	 * SocialMediaAccounts constructor.
	 *
	 * @param array $items
	 */
	public function __construct( array $items ) {
		$this->items = $items;
	}

	/**
	 * @return array
	 */
	public function requirements() {
		return [
			$this->items,
		];
	}
}
