<?php

namespace Dudley\Patterns\Pattern\SocialMediaAccounts;

use Dudley\Patterns\Abstracts\AbstractItem;

/**
 * Class SocialMediaAccountsItem
 * @package Dudley\Patterns\Pattern\SocialMediaAccounts
 */
class SocialMediaAccountsItem extends AbstractItem {
	/**
	 * Slug for the service.
	 *
	 * @var string $service
	 */
	public $service;

	/**
	 * Name of the service.
	 *
	 * @var string $service_name
	 */
	public $service_name;

	/**
	 * URL for this social media account.
	 *
	 * @var string $url
	 */
	public $url;

	/**
	 * SocialMediaAccountsItem constructor.
	 */
	public function __construct() {
		$options       = get_sub_field_object( 'social_media_account', 'options' );
		$this->service = get_sub_field( 'social_media_account', 'options' );
		$this->url     = get_sub_field( 'social_media_account_url', 'options' );

		if ( isset( $options['choices'][ $this->service ] ) ) {
			$this->service_name = $options['choices'][ $this->service ];
		}
	}

	/**
	 * @return array
	 */
	public function requirements() {
		return [
			'service'      => $this->service,
			'service_name' => $this->service_name,
			'url'          => $this->url,
		];
	}

	/**
	 * @return string
	 */
	public function url() {
		echo esc_url( $this->url );
	}

	/**
	 * @return string
	 */
	public function slug() {
		esc_attr_e( $this->service );
	}

	/**
	 * @return string
	 */
	public function name_attr() {
		esc_attr_e( $this->service_name );
	}

	/**
	 *
	 */
	public function name() {
		esc_html_e( $this->service_name );
	}
}
