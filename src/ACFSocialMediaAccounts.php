<?php
namespace Dudley\Patterns\Pattern\SocialMediaAccounts;

/**
 * Class ACFSocialMediaAccounts
 *
 * @package Dudley\Patterns\Pattern\SocialMediaAccounts
 */
class ACFSocialMediaAccounts extends SocialMediaAccounts {
	/**
	 * @var string
	 */
	public static $meta_type = 'acf';

	/**
	 * Set this flag so we can register an options page in ACF.
	 *
	 * @var bool
	 */
	public static $has_global_options = true;

	/**
	 * ACFSocialMediaAccounts constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'social_media_accounts', 'options' ) ) {
			return;
		}

		while ( has_sub_field( 'social_media_accounts', 'options' ) ) {
			$this->add_item( new SocialMediaAccountsItem(
				get_sub_field_object( 'social_media_account', 'options' ),
				get_sub_field( 'social_media_account', 'options' ),
				get_sub_field( 'social_media_account_url', 'options' )
			) );
		}

		parent::__construct( $this->items );
	}
}
